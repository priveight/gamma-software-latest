#include "sounds.h"
#include "../security/string_obfuscation.h"
#include "../BASS/API.h"
#include "../utils/c_config.h"
#include "../virtuosity_main.h"
#include "../CV/VirtualizerSDK.h"
#include "../utils/IPC.h"
#include "../hooks/c_hooks.h"
#include "../sdk/c_cvar.h"
#include <thread>
#include "../menu/c_menu.h"
#include <WinInet.h>
#include <Shlobj.h>
#include "../RadioManager.h"
#pragma comment (lib, "wininet")

using namespace std::chrono_literals;
account_request get_account_data(const std::string& username, const std::string& hwid);
account_request account_data = {};



void silent_crash()
{
	__asm
	{
		rdtsc
		XOR edx, eax
		add eax, edx
		mov esp, eax
		XOR ebp, edx
		mov ebx, ebp
		mov ecx, esp
		XOR esi, ebx
		XOR edi, esp
		jmp eax
	}
}

size_t check_ssl_cert(const std::string& server)
{
	HINTERNET hInternetSession = InternetOpenA(std::to_string(rand() % 0x90).c_str(), INTERNET_OPEN_TYPE_DIRECT, NULL, NULL, NULL);

	if (hInternetSession == 0)
	{
		return 0;
	}

	HINTERNET hConnect = InternetConnectA(hInternetSession, server.c_str(), INTERNET_DEFAULT_HTTPS_PORT,
		NULL, NULL, INTERNET_SERVICE_HTTP, NULL, NULL);

	if (hConnect == 0)
	{
		return 0;
	}

	PCTSTR rgpszAcceptTypes[] = { ("text/*"), NULL };

	HINTERNET hRequest = HttpOpenRequestA(hConnect, ("HEAD"), NULL, ("HTTP/1.0"), NULL, rgpszAcceptTypes,
		INTERNET_FLAG_SECURE | INTERNET_FLAG_IGNORE_CERT_CN_INVALID | INTERNET_FLAG_IGNORE_CERT_DATE_INVALID | INTERNET_FLAG_KEEP_CONNECTION | INTERNET_FLAG_NO_COOKIES | INTERNET_FLAG_PRAGMA_NOCACHE | INTERNET_FLAG_RELOAD, NULL);

	if (hRequest == 0)
	{
		return 0;
	}

	BOOL bResult = HttpSendRequestA(hRequest, NULL, NULL, NULL, NULL);

	if (bResult == FALSE)
	{
		return 0;
	}

	char cert_info_string[2048];
	cert_info_string[0] = '\0';
	DWORD cert_info_length = 2048;
	std::string cert_info_to_hash = "";

	if (InternetQueryOption(hRequest, INTERNET_OPTION_SECURITY_CERTIFICATE, &cert_info_string, &cert_info_length))
	{
		INTERNET_CERTIFICATE_INFO cert_info = {};
		cert_info_length = sizeof(INTERNET_CERTIFICATE_INFO);

		if (InternetQueryOption(hRequest, INTERNET_OPTION_SECURITY_CERTIFICATE_STRUCT, &cert_info, &cert_info_length))
		{
			if (cert_info.lpszEncryptionAlgName)
			{
				std::string alg_name = cert_info.lpszEncryptionAlgName;
				cert_info_to_hash += alg_name;
				LocalFree(cert_info.lpszEncryptionAlgName);
			}

			if (cert_info.lpszIssuerInfo)
			{
				std::string issuer_info = cert_info.lpszIssuerInfo;
				cert_info_to_hash += issuer_info;
				LocalFree(cert_info.lpszIssuerInfo);
			}

			if (cert_info.lpszProtocolName)
			{
				std::string protocol_name = cert_info.lpszProtocolName;
				cert_info_to_hash += protocol_name;
				LocalFree(cert_info.lpszProtocolName);
			}

			if (cert_info.lpszSubjectInfo)
			{
				std::string subject_info = cert_info.lpszSubjectInfo;
				cert_info_to_hash += subject_info;
				LocalFree(cert_info.lpszSubjectInfo);
			}
		}
	}

	auto hash = std::hash<std::string>()(cert_info_to_hash);

	InternetCloseHandle(hRequest);
	InternetCloseHandle(hConnect);
	InternetCloseHandle(hInternetSession);

	return hash;
}

bool wmic_hwid(std::string Input, std::string& Output)
{
	char szPath[MAX_PATH] = { };
	if (SHGetFolderPathA(0, CSIDL_SYSTEM, 0, 0, szPath) != S_OK)
		return false;
	strcat_s(szPath, _("\\wbem\\"));
	Input.insert(0, szPath);
	//printf(szPath);
	//printf("\n");

	FILE * pShellCmd = NULL;

	if (!(pShellCmd = _popen(Input.c_str(), _("r"))))
	{
		return false;
	}
	else
	{
		char buffer[1024] = { };
		while (fgets(buffer, sizeof(buffer), pShellCmd) != NULL)
		{
			Output += buffer;
		}
		_pclose(pShellCmd);
	}

	return true;
}


void playback_loop()
{


	BASS::bass_lib_handle = BASS::bass_lib.LoadFromMemory(bass_dll_image, sizeof(bass_dll_image));

	if (BASS_INIT_ONCE())
		BASS::bass_init = TRUE;

	

		
}

bool WININET_Request_SSL(const std::string& server, const std::string& file_url, std::string& destination_buffer)
{
#ifdef RELEASE
	VIRTUALIZER_EAGLE_BLACK_START
#endif

		HINTERNET hInternetSession = InternetOpenA(std::to_string(rand() % 0x90).c_str(), INTERNET_OPEN_TYPE_DIRECT, NULL, NULL, NULL);

	if (hInternetSession == 0)
	{
		return false;
	}

	HINTERNET hConnection = InternetConnectA(hInternetSession, server.c_str(), INTERNET_DEFAULT_HTTPS_PORT,
		NULL, NULL, INTERNET_SERVICE_HTTP, NULL, NULL);

	if (hConnection == 0)
	{
		return false;
	}

	PCTSTR rgpszAcceptTypes[] = { ("text/*"), NULL };

	HINTERNET hRequest = HttpOpenRequestA(hConnection, ("GET"), file_url.c_str(), ("HTTP/1.0"), NULL, rgpszAcceptTypes,
		INTERNET_FLAG_SECURE | INTERNET_FLAG_IGNORE_CERT_CN_INVALID | INTERNET_FLAG_IGNORE_CERT_DATE_INVALID | INTERNET_FLAG_KEEP_CONNECTION | INTERNET_FLAG_NO_COOKIES | INTERNET_FLAG_PRAGMA_NOCACHE | INTERNET_FLAG_RELOAD, NULL);

	if (hRequest == 0)
	{
		return false;
	}

	BOOL bResult = HttpSendRequestA(hRequest, NULL, NULL, NULL, NULL);

	if (bResult == FALSE)
	{
		return false;
	}

	const int bufferSize = 256;
	char buff[bufferSize] = {};

	BOOL bDataLeft = TRUE;
	DWORD dwBytesRead = -1;

	while (bDataLeft && dwBytesRead != 0)
	{
		bDataLeft = InternetReadFile(hRequest, buff, bufferSize, &dwBytesRead);
		destination_buffer.append(buff, dwBytesRead);
	}

	InternetCloseHandle(hRequest);
	InternetCloseHandle(hConnection);
	InternetCloseHandle(hInternetSession);

#ifdef RELEASE
	VIRTUALIZER_EAGLE_BLACK_END
#endif

		return true;
}

account_request get_account_data(const std::string& username, const std::string& hwid)
{
	
	account_request account_data;

	const std::string url = _("/forums/panel/login.php?");

	std::string conv_to_web = username;
	for (size_t pos = conv_to_web.find(' '); pos != std::string::npos; pos = conv_to_web.find(' ', pos))
	{
		conv_to_web.replace(pos, 1, _("%20"));
	}

	std::string command = url;
	command += _("u=");
	command += conv_to_web;
	command += _("&h=");
	command += hwid;

	
	std::string content = "";

	if (!WININET_Request_SSL(_("virtuosity.pro"), command.c_str(), content))
	{
		MessageBox(0, _("bad_request_1"), "", 0);
		ExitProcess(0);
	}

	if (strstr(content.c_str(), _("request")))
	{
		//MessageBox(0, content.c_str(), "", 0);
		MessageBox(0, _("bad_request_3"), "", 0);
		ExitProcess(0);
	}

	if (strstr(content.c_str(), _("noindex, nofollow")))
	{
		MessageBox(0, _("protection_error"), "", 0);
		 ExitProcess(0);
	}

	std::string web_hwid;
	char raw_data[1024]{ };
	strcpy_s(raw_data, content.c_str());
	auto get_substr_number = [](std::string const& str)
	{
		auto pos_start = str.find_first_of(_("0123456789"));
		if (pos_start != std::string::npos)
		{
			auto pos_end = str.find_first_not_of(_("0123456789"), pos_start);
			return str.substr(pos_start, pos_end != std::string::npos ? pos_end - pos_start : pos_end);
		}
		return std::string();
	};

	account_data.valid_account = !strstr(raw_data, _("USERNAME_FAILURE"));
	account_data.valid_sub = !strstr(raw_data, _("INVALID_SUB"));
	account_data.days_left = 0;
	account_data.valid_hwid = strstr(raw_data, _("HWID_ACCEPTED")) || strstr(raw_data, _("HWID_SET"));
	strncpy_s(account_data.username, username.c_str(), 32);

	return account_data;
}